package edu.calpoly.ipachev.rdd

import scala.reflect.ClassTag

import edu.calpoly.ipachev.gpu.GPUMapper
import edu.calpoly.ipachev.gpu.GPUFunction1
//import scala.runtime.AbstractFunction1;

import org.apache.spark.{Partition, TaskContext}
import org.apache.spark.rdd.RDD

/**
 * An RDD that applies the provided function to every partition of the parent RDD.
 */
private class GPUMapPartitionsRDD[U: ClassTag, T: ClassTag](
    var prev: RDD[T],
    g: (T => U),
    preservesPartitioning: Boolean = false)
  extends RDD[U](prev) {

  override val partitioner = if (preservesPartitioning) firstParent[T].partitioner else None

  override def getPartitions: Array[Partition] = firstParent[T].partitions

  override def compute(split: Partition, context: TaskContext): Iterator[U] = {
    val arr: Array[T] = firstParent[T].iterator(split, context).toArray
    val mapper: GPUMapper[U, T] =
      new GPUMapper[U, T](arr.asInstanceOf[Array[Object]], new GPUFunction1[T, U]() {
        def apply(in: T): U = g(in)
      })

    mapper.getResult().iterator
  }

  override def clearDependencies() {
    super.clearDependencies()
    prev = null
  }
}
