package edu.calpoly.ipachev.rdd

import scala.reflect.ClassTag

import org.apache.spark.rdd.RDD


class GPURDD[T: ClassTag](
  var parent: RDD[T]
) {

  def map[U: ClassTag](f: T => U): RDD[U] = {
    new GPUMapPartitionsRDD[U, T](parent, f)

  }
}
