package edu.calpoly.ipachev.gpu

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import org.scalatest.Matchers._

import scala.runtime.AbstractFunction1;

class GPUMapperSuite extends FunSuite with BeforeAndAfter {
 var arr: Array[String] = _
 var correctArr: Array[String] = _
 var func: (String => String) = _
 var javaFunc: AbstractFunction1[String, String] = _
 var mapper: GPUMapper[String, String] = _

 before {
   arr = Array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l")
   func = (s => s + "test")
   javaFunc = new AbstractFunction1[String, String] {
     def apply(in: String): String = func(in)
   }
   correctArr = arr.map(func)
 }

 test("constructor") {
   mapper = new GPUMapper[String, String](arr.asInstanceOf[Array[Object]], javaFunc)
   mapper.getInput shouldBe arr
   assert(mapper.getFunc.equals(javaFunc))
 }

 test("result") {
   mapper = new GPUMapper(arr.asInstanceOf[Array[Object]], javaFunc)
   mapper.getResult shouldBe correctArr
 }

}