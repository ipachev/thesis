package edu.calpoly.ipachev.gpu;

import org.trifort.rootbeer.runtime.Kernel;

import edu.calpoly.ipachev.gpu.GPUFunction1;


public class GPUMapperKernel<T, U> implements Kernel {

    private T[] input;
    private U[] result;
    private int index;
    private GPUFunction1<T, U> func;

    public GPUMapperKernel(T[] in, U[] out, GPUFunction1<T, U> f, int idx) {
        input = in;
        index = idx;
        result = out;
        func = f;
    }

    public void gpuMethod() {
        result[index] = func.apply(input[index]);
    }

}
