package edu.calpoly.ipachev.gpu;

import org.trifort.rootbeer.runtime.Kernel;
import org.trifort.rootbeer.runtime.Rootbeer;

import java.util.ArrayList;


public class GPUMapper<U, T> {

    private T[] input;
    private U[] result;
    private GPUFunction1<T, U> func;
    private ArrayList<Kernel> tasks;

    @SuppressWarnings("unchecked")
    public GPUMapper(Object[] in, GPUFunction1 f) {
        input = (T[])in;
        func = f;
        result = (U[])new Object[input.length];
        tasks = new ArrayList<>();
        for(int i = 0; i < input.length; i++) {
             tasks.add(new GPUMapperKernel(input, result, func, i));
        }
    }

    public T[] getInput() {
        return input;
    }

    public GPUFunction1<T, U> getFunc() {
        return func;
    }

    public U[] getResult() {
        Rootbeer rb = new Rootbeer();
        rb.run(tasks);
        return result;
    }
}