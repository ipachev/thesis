package edu.calpoly.ipachev.gpu;

public interface GPUFunction1<T, U> {
    public U apply(T param);
}
