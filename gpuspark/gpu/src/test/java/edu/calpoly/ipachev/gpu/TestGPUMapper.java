package edu.calpoly.ipachev.gpu;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestGPUMapper {

    private GPUFunction1<String, String> func;
    private String[] arr;
    private String[] correctArr;
    private GPUMapper<String, String> mapper;

    private String testString = "taco";

    @Before
    public void setUp() {
        func = new GPUFunction1<String, String>() {
            @Override
            public String apply(String s) {
                return s + testString;
            }
        };

        arr = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l"};
        correctArr = new String[arr.length];
        for(int i = 0; i < arr.length; i++) {
            correctArr[i] = arr[i] + testString;
        }
    }

    @Test
    public void testConstructor() {
        mapper = new GPUMapper<>(arr, func);
        Assert.assertArrayEquals(arr, mapper.getInput());
        Assert.assertEquals(func, mapper.getFunc());
    }

    @Test
    public void testResult() {
        mapper = new GPUMapper<>(arr, func);
        Object[] resultArr = mapper.getResult();
        for(int i = 0; i < correctArr.length; i++) {
            Assert.assertEquals(correctArr[i], resultArr[i]);
        }

    }
}
