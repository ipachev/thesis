import edu.calpoly.ipachev.rdd.GPURDD

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object SimpleApp {
    def main(args: Array[String]) {
        val conf = new SparkConf().setAppName("Simple Application")
        val sc = new SparkContext(conf)
        val rdd = sc.parallelize(Array("taco", "taco", "taco", "taco", "taco", "taco", "taco", "taco", "burrito"))
        val gpurdd = new GPURDD(rdd)
        val mapped = gpurdd.map(s => s + "meow")
        mapped.collect().foreach(println)
    }
}

